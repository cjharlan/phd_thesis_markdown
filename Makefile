# Made for Windows
PY=python
PANDOC=pandoc

BASEDIR=$(CURDIR)
INPUTDIR=$(BASEDIR)/source
OUTPUTDIR=$(BASEDIR)/output
TEMPLATEDIR=$(INPUTDIR)/templates
STYLEDIR=$(BASEDIR)/style
SCRATCHDIR=$(BASEDIR)/scratch
TEMPLATE=${STYLEDIR}/GSBS_template.tex
REFERENCESTYLE=$(STYLEDIR)/nature-no-et-al.csl

BIBFILE=$(INPUTDIR)/references.bib
# all: pdf tex html docx
all: pdf tex docx
help:
	@echo ''
	@echo 'Makefile for the Markdown thesis'
	@echo ''
	@echo 'Usage:'
	@echo '   make install                     install pandoc plugins'
	@echo '   make html                        generate a web version'
	@echo '   make pdf                         generate a PDF file'
	@echo '   make docx                        generate a Docx file'
	@echo '   make tex                         generate a Latex file'
	@echo '   multiline_tables                 convert a large markdown table into a$\'
	@echo '									                                        multi-line format' 
	@echo ' 																	  '
	@echo '   tables	                       convert a md table into LaTeX, to$\'
	@echo '                                       be used sideways or with other$\'
	@echo '      			                       advanced formatting'@echo ''
	@echo ''
	@echo 'get local templates with: pandoc -D latex/html/etc'
	@echo 'or generic ones from: https://github.com/jgm/pandoc-templates'

# ifeq ($(OS),Windows_NT) 
# 	@echo '$(OS)'
# 	detected_OS=Windows
# else
# 	detected_OS=$(shell sh -c 'uname 2>/dev/null || echo Unknown')
# endif

# UNAME := $(shell uname)
# ifeq ($(UNAME), Linux)
# install:
# 	bash $(BASEDIR)/install_linux.sh
# else ifeq ($(shell uname), Darwin)
# install:
# 	bash $(BASEDIR)/install_mac.sh
# endif


pdf:
	pandoc  \
		--output "$(OUTPUTDIR)/thesis.pdf" \
		--template="$(TEMPLATE)" \
		--include-in-header="$(STYLEDIR)/preamble.tex" \
		--include-after-body="$(INPUTDIR)/vita.tex"\
		--variable=fontsize:12pt \
		--variable=papersize:letterpaper \
		--variable=documentclass:report \
		--pdf-engine=xelatex \
		$(sort $(wildcard $(INPUTDIR)/*.md)) \
		"$(INPUTDIR)/metadata.yml" \
		--filter=pandoc-shortcaption \
		--filter=pandoc-xnos \
		--bibliography="$(BIBFILE)" \
		--citeproc \
		--csl=$(REFERENCESTYLE) \
		--number-sections \
		--verbose \
		2>$(OUTPUTDIR)/pandoc.pdf.log

tex:
	pandoc  \
		--output "$(OUTPUTDIR)/thesis.tex" \
		--template="$(TEMPLATE)" \
		--include-in-header="$(STYLEDIR)/preamble.tex" \
		--include-after-body="$(INPUTDIR)/vita.tex"\
		--variable=fontsize:12pt \
		--variable=papersize:letterpaper \
		--variable=documentclass:report \
		--pdf-engine=xelatex \
		$(sort $(wildcard $(INPUTDIR)/*.md)) \
		"$(INPUTDIR)/metadata.yml" \
		--filter=pandoc-shortcaption \
		--filter=pandoc-xnos \
		--bibliography="$(BIBFILE)" \
		--citeproc \
		--csl=$(REFERENCESTYLE) \
		--number-sections \
		--verbose \
		2>$(OUTPUTDIR)/pandoc.tex.log

html:
	pandoc  \
		--output "$(OUTPUTDIR)/thesis.html" \
		--template="$(STYLEDIR)/template.html" \
		--include-in-header="$(STYLEDIR)/style.css" \
		--toc --mathjax\
		$(sort $(wildcard $(INPUTDIR)/*.md)) \
		"$(INPUTDIR)/metadata.yml" \
		--filter=pandoc-shortcaption \
		--filter=pandoc-xnos \
		--bibliography="$(BIBFILE)" \
		--citeproc \
		--csl=$(REFERENCESTYLE) \
		--number-sections \
		--verbose \
		2>$(OUTPUTDIR)/pandoc.html.log
	rd /s "$(OUTPUTDIR)/source" /s/q
	md "$(OUTPUTDIR)/source"
	xcopy "$(INPUTDIR)/figures" "$(OUTPUTDIR)/source/figures" /e/h/i

docx:
	pandoc  \
		--output "$(OUTPUTDIR)/thesis.docx" \
		--toc --webtex --mathjax\
		$(sort $(wildcard $(INPUTDIR)/*.md)) \
		"$(INPUTDIR)/metadata.yml" \
		--filter=pandoc-shortcaption \
		--filter=pandoc-xnos \
		--bibliography="$(BIBFILE)" \
		--citeproc \
		--csl=$(REFERENCESTYLE) \
		--number-sections \
		--verbose \
		2>$(OUTPUTDIR)/pandoc.docx.log

multiline_tables:
	pandoc "$(SCRATCHDIR)/tables.md" \
	-t markdown+multiline_tables \
	-o "$(SCRATCHDIR)/cleaned_tables.md"

tables: multiline_tables
	pandoc "$(SCRATCHDIR)/cleaned_tables.md" \
	-o "$(SCRATCHDIR)/tables.tex"


.PHONY: help install pdf docx html tex multiline_tables tables
